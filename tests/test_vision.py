from pathlib import Path
import cv2
import pytest
import loopfinder.vision as lib


def all_test_images():
    return [
        (path, cv2.imread(str(path))) for path in Path("./tests/res").glob("*.jpeg")
    ]


def test_find_loop_empty():
    img = cv2.imread("./tests/res/empty.jpeg")
    pos = lib.find_loop(img)
    assert pos is None


def test_find_loop():
    img = cv2.imread("./tests/res/loop.jpeg")
    h, w = img.shape[:2]
    pos = lib.find_loop(img)
    assert pos is not None
    x, y = pos
    assert h > x >= 0
    assert w > y >= 0


def test_loop_below():
    img = cv2.imread("./tests/res/pin.jpeg")
    pos = lib.find_loop(img)
    assert pos
    _, y = pos
    assert y == img.shape[0] - 1


@pytest.mark.parametrize("masker", [lib.mini_canny_masker, lib.canny_masker])
def test_canny_mask(masker):
    from pathlib import Path

    outdir = Path(f"./tests/arti/{masker.__name__}")
    outdir.mkdir(exist_ok=True, parents=True)
    for path, img_in in all_test_images():
        img_in = cv2.cvtColor(img_in, cv2.COLOR_BGR2GRAY)
        img_out = masker(img_in)
        cv2.imwrite(str(outdir / path.name), img_out)
