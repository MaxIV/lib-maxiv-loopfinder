###############################################################################
# This file is part of the lib-maxiv-loopfinder project.
#
# Copyright Lund University
#
# Distributed under the GNU GPLv3 license. See LICENSE file for more info.
###############################################################################
import pytest
import cv2
from loopfinder import motion as lib
from unittest.mock import MagicMock


def test_top_level_import_has_version():
    import loopfinder as package

    assert hasattr(package, "__version__")


def test_navigation_empty():
    img = cv2.imread("./tests/res/empty.jpeg")
    navigator = lib.CentringNavigator(
        target_coordinates=(img.shape[1] // 2, img.shape[0] // 2), tolerance=12
    )
    res = navigator.next_step(img)
    assert not res.finished()
    assert res.rotate == 70
    assert res.x_to_center == img.shape[1] // 2
    assert res.y_to_center == img.shape[0]


def test_navigation_mtb():
    img = cv2.imread("./tests/res/loop.jpeg")
    navigator = lib.CentringNavigator(
        target_coordinates=(img.shape[1] // 2, img.shape[0] // 2), tolerance=12
    )
    res = navigator.next_step(img)
    assert res.rotate is None
    assert res.x_to_center is not None
    assert res.y_to_center is not None


@pytest.mark.parametrize("name", ["blurry_low", "blurry_lower", "pin"])
def test_navigation_below(name):
    """In these images, the loop is below or at the bottom edge."""
    img = cv2.imread(f"./tests/res/{name}.jpeg")
    navigator = lib.CentringNavigator(
        target_coordinates=(img.shape[1] // 2, img.shape[0] // 2), tolerance=12
    )
    res = navigator.next_step(img)
    assert res.rotate is None
    assert res.y_to_center == img.shape[0] - 1


def test_full_navigation():
    import loopfinder.motion as m
    import numpy as np

    black_img = np.zeros((100, 100, 3))
    m.find_loop = MagicMock()
    navigator = m.CentringNavigator(target_coordinates=(50, 50), tolerance=5)

    # I cant see anything!
    m.find_loop.return_value = None
    step = navigator.next_step(black_img)
    assert not step.finished()
    assert step.rotate == 70
    assert step.y_to_center is not None

    # There's something! move there!
    m.find_loop.return_value = (10, 5)
    step = navigator.next_step(black_img)
    assert not step.finished()
    assert step.rotate is None
    assert step.x_to_center == 10
    assert step.y_to_center == 5

    # ok, now it's in the center, let's check the z axis as well
    m.find_loop.return_value = (50, 50)
    step = navigator.next_step(black_img)
    assert not step.finished()
    assert step.rotate == 90

    # its a little off to the side, go there.
    m.find_loop.return_value = (40, 50)
    step = navigator.next_step(black_img)
    assert not step.finished()
    assert step.rotate is None
    assert step.x_to_center == 40
    assert step.y_to_center == 50

    # ok, that looks good, rotate and make sure we're centered
    m.find_loop.return_value = (50, 50)
    step = navigator.next_step(black_img)
    assert not step.finished()
    assert step.rotate == 90

    # finished!
    m.find_loop.return_value = (50, 50)
    step = navigator.next_step(black_img)
    assert step.finished()


def test_finished():
    from loopfinder.motion import Step

    assert Step().finished()
    assert not Step(rotate=1).finished()
    assert not Step(rotate=0).finished()
