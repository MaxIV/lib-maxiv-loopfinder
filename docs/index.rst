.. lib-maxiv-loopfinder documentation master file, created by
   sphinx-quickstart on Thu Mar 15 16:47:29 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to lib-maxiv-loopfinder documentation!
==========================================

Contents:

.. toctree::
   :maxdepth: 2

    loopfinder <loopfinder/loopfinder>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

